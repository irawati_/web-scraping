package ira.scraping.tokped;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;
import ira.scraping.tokped.exceptions.GeneralExceptions;
import ira.scraping.tokped.model.DetailProduct;
import ira.scraping.tokped.model.DetailProduct.Component;
import ira.scraping.tokped.model.ResponseBody;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class IraScrapingTokped {

    public static void main(String[] args) {

        var res = callToClient();
        try {
            ObjectMapper mapper = new ObjectMapper().configure(
                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            ResponseBody[] responses = mapper.readValue(res, ResponseBody[].class);

            List<String[]> data = new ArrayList<>();
            data.add(new String[]{"PRODUCT_NAME", "PRICE", "IMAGE_URL", "RATING", "MERCHANT"});

            List<ResponseBody> responseList = Arrays.asList(responses);
            responseList.forEach(r -> r.getData().getAceSearchProductV4().getData().getProducts()
                .forEach(p -> {
                    String[] url = p.getUrl().split("/");
                    String shop = url[3];
                    String[] product = url[4].split("\\?");
                    String productKey = product[0];

                    String description = callToDetailProduct(shop, productKey);

                    data.add(new String[]{p.getName(), p.getPrice(), p.getImageUrl(),
                        p.getRating().toString(), p.getShop().getName()});
                }));

            exportCsv(data);
        } catch (JsonProcessingException e) {
            throw new GeneralExceptions(e.getMessage(), e);
        }
    }

    private static String callToClient() {
        String uri = "https://gql.tokopedia.com/graphql/SearchProductQueryV4";
        String query = "[{\"operationName\":\"SearchProductQueryV4\","
            + "\"variables\":{\"params\":\"device=desktop&ob=9&page=1&q=hanphone&related=true&rf"
            + "=true&rows=100&safe_search=false&sc=24&scheme=https&shipping=&source=search&st"
            + "=product&start=0&topads_bucket=true&unique_id=1b0c7d353866c4b6fb4594429dd82840&user_addressId=&user_cityId=176&user_districtId=2274&user_id=&user_lat=&user_long=&user_postCode=&user_warehouseId=12210375&variants=\"},\"query\":\"query SearchProductQueryV4($params: String!) {\\n  ace_search_product_v4(params: $params) {\\n    header {\\n      totalData\\n      totalDataText\\n      processTime\\n      responseCode\\n      errorMessage\\n      additionalParams\\n      keywordProcess\\n      componentId\\n      __typename\\n    }\\n    data {\\n      banner {\\n        position\\n        text\\n        imageUrl\\n        url\\n        componentId\\n        trackingOption\\n        __typename\\n      }\\n      backendFilters\\n      isQuerySafe\\n      ticker {\\n        text\\n        query\\n        typeId\\n        componentId\\n        trackingOption\\n        __typename\\n      }\\n      redirection {\\n        redirectUrl\\n        departmentId\\n        __typename\\n      }\\n      related {\\n        position\\n        trackingOption\\n        relatedKeyword\\n        otherRelated {\\n          keyword\\n          url\\n          product {\\n            id\\n            name\\n            price\\n            imageUrl\\n            rating\\n            countReview\\n            url\\n            priceStr\\n            wishlist\\n            shop {\\n              city\\n              isOfficial\\n              isPowerBadge\\n              __typename\\n            }\\n            ads {\\n              adsId: id\\n              productClickUrl\\n              productWishlistUrl\\n              shopClickUrl\\n              productViewUrl\\n              __typename\\n            }\\n            badges {\\n              title\\n              imageUrl\\n              show\\n              __typename\\n            }\\n            ratingAverage\\n            labelGroups {\\n              position\\n              type\\n              title\\n              url\\n              __typename\\n            }\\n            componentId\\n            __typename\\n          }\\n          componentId\\n          __typename\\n        }\\n        __typename\\n      }\\n      suggestion {\\n        currentKeyword\\n        suggestion\\n        suggestionCount\\n        instead\\n        insteadCount\\n        query\\n        text\\n        componentId\\n        trackingOption\\n        __typename\\n      }\\n      products {\\n        id\\n        name\\n        ads {\\n          adsId: id\\n          productClickUrl\\n          productWishlistUrl\\n          productViewUrl\\n          __typename\\n        }\\n        badges {\\n          title\\n          imageUrl\\n          show\\n          __typename\\n        }\\n        category: departmentId\\n        categoryBreadcrumb\\n        categoryId\\n        categoryName\\n        countReview\\n        customVideoURL\\n        discountPercentage\\n        gaKey\\n        imageUrl\\n        labelGroups {\\n          position\\n          title\\n          type\\n          url\\n          __typename\\n        }\\n        originalPrice\\n        price\\n        priceRange\\n        rating\\n        ratingAverage\\n        shop {\\n          shopId: id\\n          name\\n          url\\n          city\\n          isOfficial\\n          isPowerBadge\\n          __typename\\n        }\\n        url\\n        wishlist\\n        sourceEngine: source_engine\\n        __typename\\n      }\\n      violation {\\n        headerText\\n        descriptionText\\n        imageURL\\n        ctaURL\\n        ctaApplink\\n        buttonText\\n        buttonType\\n        __typename\\n      }\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\"}]";

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(uri))
            .setHeader("authority", "gql.tokopedia.com").setHeader("accept", "*/*")
            .setHeader("accept-language", "en-US,en;q=0.9")
            .setHeader("content-type", "application/json").setHeader("cookie",
                "bm_sz=E92B20F05130B84B96078C387D5745CF~YAAQnewZuBEvQcmCAQAAtQm43xAlycb10nfo5Xc1j72lebDZbPEC1PYcD17Bp+CAnPSMmRxieaAoorVbRyhoYSlPRX+k880iKnIzxel1i5g0xNvHevyy4f61znS2JAPKsifsAVLd+IhqQzqSrqoC8QdNSMrD8JgOobHoYkIOFzwPOQQBbKUeImM1kN5qosqrv/lKJH8bjMJ8CY9Qc1ETPc6glqQ8pv3uWg8eX0Gg+eHkixETr9wdZM73wjZxgJETO/z9PYSgYHK22i91TjkSIPb1Bnq9H20stIwqV5tI/vmnBooyd40=~3359288~4600369; bm_mi=BE0A3AF324093335550A5A55AACAD23E~YAAQnewZuFUvQcmCAQAAbxi43xDVd5LXoRf3Y1x8NpE9BINIbSIsUcW9fvEqdDGtrAa1PznbD2t/QH+r2N4mnYhysUizB2GX6dTnqfHeRNT2whZ3OvoIXaQ0g0v3yJn3Hd1BQyOcpSR+yeWnartaLi6WWNA9GMgH9VVPD9mXgCj767JV4W5iMW8Z/wkyIp+rAbxQpZwBXwzKqj4t2Z9uzDxNpOl6IhywY9wXBT1MtliVv4y6eLNgPeb8pbwC2V4zT6CF5Fim3a0XjqWgL79F5cNwcgrWZxmRGsVJCNzpsDVuD3hXd04IqX6WOCwqa1a+TQ==~1; _SID_Tokopedia_=8JKh_lbnxUWjAqZjHaFQMOJKKduRauawskIKwTEwZH3XNAFOTV8G-U48A-eqvhRYBtwR8YAypwMHCPgW-I7D_vyAEQuqxWAvRcv-y2FXSbfBm-M3GtDwkJvPz1bS4xCN; DID=a80b7a718680c7f198b2d8b5f3eefd2752cace9f7008235d810787cc105cb547c76d53c2300b9f27408ab141019b48b5; DID_JS=YTgwYjdhNzE4NjgwYzdmMTk4YjJkOGI1ZjNlZWZkMjc1MmNhY2U5ZjcwMDgyMzVkODEwNzg3Y2MxMDVjYjU0N2M3NmQ1M2MyMzAwYjlmMjc0MDhhYjE0MTAxOWI0OGI147DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=; _gcl_au=1.1.702490648.1661610763; _UUID_NONLOGIN_=de04eeb262bbb898c1bfc3fe8037387c; hfv_banner=true; _gid=GA1.2.413608813.1661610763; _UUID_CAS_=f3d79553-1c23-4278-b4d8-3f812ac69331; _CASE_=752c6a476a2c343c3c393a222c6f476a2c343e222c626c622c342c446f656f7c7a6f2e5e7b7d6f7a2c222c6d476a2c343f3938222c626160692c342c2c222c626f7a2c342c2c222c7e4d612c342c2c222c79476a2c343f3c3c3f3e3d393b222c7d476a2c343f3f3b3d3e3b393d222c7d5a777e6b2c342c3c662c222c79667d2c342c5575522c796f7c6b66617b7d6b51676a522c343f3c3c3f3e3d393b22522c7d6b7c78676d6b517a777e6b522c34522c3c66522c22522c51517a777e6b606f636b522c34522c596f7c6b66617b7d6b7d522c732275522c796f7c6b66617b7d6b51676a522c343e22522c7d6b7c78676d6b517a777e6b522c34522c3f3b63522c22522c51517a777e6b606f636b522c34522c596f7c6b66617b7d6b7d522c73532c222c625b7e6a2c342c3c3e3c3c233e36233c395a3c3f343d3c343a3d253e39343e3e2c73; ak_bmsc=5C8EFF87BE8CEAFA98A3800FE1371CE9~000000000000000000000000000000~YAAQnewZuJYvQcmCAQAAqii43xD8SUR+cmzpaQC4F+cF5oASCcGKj7x4BGtIM7ZmLRuzFHReESyi/cqlLYOAwFQUiZXF+xGVvnkjRt0FLK3vGRue/4onD0zzuOTN2GEXK6ru4+PsJlLM8aFfCaEBma+UVuq8NSegulCR42OCf+e01h++3LtVVm2wygAIWZ4riFrNJukj42uy9cHoFo6JFVcmu2S0NtLluONLjVaIJ3RF2unZKopCZPAciBbYCw9L1Soo3vXBKqekb8+cJl4pxcBWphZTdq0b/Qw3lWdssK3XOQA3ij8t6sRl4gZ838sL11a093aApJSDgT+ymzwYnsa1WRk96LO88uFZi2/tYm5hQAyIQMlG7zpTNrpfO809FilZaOCJlPPMVD2w3RwZ0wwReI5fmGX8GIGHDpT8WY4WtMPfFCzyvIjRcUZ98iSBXfsUB6LsfCPykQa8cMP0a80JMybnuiWa0cB18HG3IXp7DDRwi88CuNOaW+HnTEfpPxQD; _jxx=1d96bc60-2615-11ed-82cc-d9794eebe228; _jxxs=1661610765-1d96bc60-2615-11ed-82cc-d9794eebe228; _jx=1d96bc60-2615-11ed-82cc-d9794eebe228; _jxs=1661610765-1d96bc60-2615-11ed-82cc-d9794eebe228; __asc=a752077a182dfb838bc9a5cba5c; __auc=a752077a182dfb838bc9a5cba5c; _fbp=fb.1.1661610823625.323288658; bm_sv=75B577433CE2976C2C92C66AE6A48457~YAAQfMMmFx+T79KCAQAASpjQ3xDbB7yu3rMVgNprpSsR8eI5wJl6J6/cfXDexjxHZhlNGN5i7FKYqqq7FLzgTwSXOROf1tengUJrmXenPhpMHwxMDsx/0iqXEGNdbRImnO+AXhbjz00vU/AUg9NpOR7zEIoQVMMj44nATVnEsOP//OSfAxiE5FHvF6xcS0Sj5ejHq+CaKSxtZBIshzUQfn5z4GW1vjikAuYZm3tAjGXEWVcHD7Q/OQ7/rdXvUNJqit+H~1; _abck=D5F8B7C9D190F13E85F23F9E332B9DA3~0~YAAQrewZuEiAs9qCAQAA+aLQ3wi9XwRPuiCdN94VhDiAq41rQppE5anFI4t+7/q7AZPQiq/PWR6Z/tpyp+UfRS/3RbrPLg59EHbYdugrPPZxBVCrxir1cjzy0mfZav8G06dUWjdZgW3+jMoTcjpvWKOyNxtoCmRcwthYn70jhM9swbJ6HhTE0IlriXiS2Vvy+MRENQzcdpGuV6PRw3rh/Bx3niSqgdPMTgRPl8cQBgB22QTFAApWI6eWHmul2crxGhDvDV8cJQT131Rk23wZ4sGEN08/9U6N9G6UkfJ+UcoJiWCKmme3Uym1zlR2F+xFo1uP6YjgY5sxIADjhE+UkqH8DoZ0Lug0VdV8Tg5+Af6uA6qhRBMMaUV+6/LhOd1VYJrz3DCbhYGX2V3rq2vf9AgMbkf26zd4nGo=~-1~-1~-1; _ga=GA1.2.629140000.1661610763; _gat_UA-9801603-1=1; _dc_gtm_UA-126956641-6=1; _dc_gtm_UA-9801603-1=1; _ga_70947XW48P=GS1.1.1661610763.1.1.1661612783.32.0.0")
            .setHeader("origin", "https://www.tokopedia.com").setHeader("referer",
                "https://www.tokopedia.com/search?navsource=&rf=true&sc=24&srp_component_id=02.01.00.00&srp_page_id=&srp_page_title=&st=product&q=hanphone")
            .setHeader("sec-ch-ua",
                "\"Chromium\";v=\"104\", \" Not A;Brand\";v=\"99\", \"Google Chrome\";v=\"104\"")
            .setHeader("sec-ch-ua-mobile", "?0").setHeader("sec-ch-ua-platform", "\"Linux\"")
            .setHeader("sec-fetch-dest", "empty").setHeader("sec-fetch-mode", "cors")
            .setHeader("sec-fetch-site", "same-site").setHeader("tkpd-userid", "0")
            .setHeader("user-agent",
                "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36")
            .setHeader("x-device", "desktop-0.0").setHeader("x-source", "tokopedia-lite")
            .setHeader("x-tkpd-lite-service", "zeus").setHeader("x-version", "d90a3a7")
            .POST(HttpRequest.BodyPublishers.ofString(query))
            .timeout(Duration.of(30, ChronoUnit.SECONDS)).build();

        try {
            HttpResponse<String> response = httpClient.send(request,
                HttpResponse.BodyHandlers.ofString());

            return response.body();
        } catch (IOException | InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new GeneralExceptions(e.getMessage(), e);
        }


    }


    private static String callToDetailProduct(String shop, String productKey) {

        String uri = "https://gql.tokopedia.com/graphql/PDPGetLayoutQuery";
        String payload = "[{\"operationName\":\"PDPGetLayoutQuery\","
            + "\"variables\":{\"shopDomain\":\"" + shop + "\",\"productKey"
            + "\":\"" + productKey + "\",\"layoutID\":\"\",\"apiVersion\":1,"
            + "\"userLocation\":{\"cityID\":\"176\",\"addressID\":\"0\",\"districtID\":\"2274\",\"postalCode\":\"\",\"latlon\":\"\"},\"extParam\":\"ivf%3Dfalse%26src%3Dsearch\"},\"query\":\"fragment ProductVariant on pdpDataProductVariant {\\n  errorCode\\n  parentID\\n  defaultChild\\n  sizeChart\\n  totalStockFmt\\n  variants {\\n    productVariantID\\n    variantID\\n    name\\n    identifier\\n    option {\\n      picture {\\n        urlOriginal: url\\n        urlThumbnail: url100\\n        __typename\\n      }\\n      productVariantOptionID\\n      variantUnitValueID\\n      value\\n      hex\\n      stock\\n      __typename\\n    }\\n    __typename\\n  }\\n  children {\\n    productID\\n    price\\n    priceFmt\\n    optionID\\n    productName\\n    productURL\\n    picture {\\n      urlOriginal: url\\n      urlThumbnail: url100\\n      __typename\\n    }\\n    stock {\\n      stock\\n      isBuyable\\n      stockWordingHTML\\n      minimumOrder\\n      maximumOrder\\n      __typename\\n    }\\n    isCOD\\n    isWishlist\\n    campaignInfo {\\n      campaignID\\n      campaignType\\n      campaignTypeName\\n      campaignIdentifier\\n      background\\n      discountPercentage\\n      originalPrice\\n      discountPrice\\n      stock\\n      stockSoldPercentage\\n      startDate\\n      endDate\\n      endDateUnix\\n      appLinks\\n      isAppsOnly\\n      isActive\\n      hideGimmick\\n      isCheckImei\\n      minOrder\\n      __typename\\n    }\\n    thematicCampaign {\\n      additionalInfo\\n      background\\n      campaignName\\n      icon\\n      __typename\\n    }\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment ProductMedia on pdpDataProductMedia {\\n  media {\\n    type\\n    urlOriginal: URLOriginal\\n    urlThumbnail: URLThumbnail\\n    videoUrl: videoURLAndroid\\n    prefix\\n    suffix\\n    description\\n    __typename\\n  }\\n  videos {\\n    source\\n    url\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment ProductHighlight on pdpDataProductContent {\\n  name\\n  price {\\n    value\\n    currency\\n    __typename\\n  }\\n  campaign {\\n    campaignID\\n    campaignType\\n    campaignTypeName\\n    campaignIdentifier\\n    background\\n    percentageAmount\\n    originalPrice\\n    discountedPrice\\n    originalStock\\n    stock\\n    stockSoldPercentage\\n    threshold\\n    startDate\\n    endDate\\n    endDateUnix\\n    appLinks\\n    isAppsOnly\\n    isActive\\n    hideGimmick\\n    __typename\\n  }\\n  thematicCampaign {\\n    additionalInfo\\n    background\\n    campaignName\\n    icon\\n    __typename\\n  }\\n  stock {\\n    useStock\\n    value\\n    stockWording\\n    __typename\\n  }\\n  variant {\\n    isVariant\\n    parentID\\n    __typename\\n  }\\n  wholesale {\\n    minQty\\n    price {\\n      value\\n      currency\\n      __typename\\n    }\\n    __typename\\n  }\\n  isCashback {\\n    percentage\\n    __typename\\n  }\\n  isTradeIn\\n  isOS\\n  isPowerMerchant\\n  isWishlist\\n  isCOD\\n  isFreeOngkir {\\n    isActive\\n    __typename\\n  }\\n  preorder {\\n    duration\\n    timeUnit\\n    isActive\\n    preorderInDays\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment ProductCustomInfo on pdpDataCustomInfo {\\n  icon\\n  title\\n  isApplink\\n  applink\\n  separator\\n  description\\n  __typename\\n}\\n\\nfragment ProductInfo on pdpDataProductInfo {\\n  row\\n  content {\\n    title\\n    subtitle\\n    applink\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment ProductDetail on pdpDataProductDetail {\\n  content {\\n    title\\n    subtitle\\n    applink\\n    showAtFront\\n    isAnnotation\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment ProductDataInfo on pdpDataInfo {\\n  icon\\n  title\\n  isApplink\\n  applink\\n  content {\\n    icon\\n    text\\n    __typename\\n  }\\n  __typename\\n}\\n\\nfragment ProductSocial on pdpDataSocialProof {\\n  row\\n  content {\\n    icon\\n    title\\n    subtitle\\n    applink\\n    type\\n    rating\\n    __typename\\n  }\\n  __typename\\n}\\n\\nquery PDPGetLayoutQuery($shopDomain: String, $productKey: String, $layoutID: String, $apiVersion: Float, $userLocation: pdpUserLocation, $extParam: String) {\\n  pdpGetLayout(shopDomain: $shopDomain, productKey: $productKey, layoutID: $layoutID, apiVersion: $apiVersion, userLocation: $userLocation, extParam: $extParam) {\\n    name\\n    pdpSession\\n    basicInfo {\\n      alias\\n      isQA\\n      id: productID\\n      shopID\\n      shopName\\n      minOrder\\n      maxOrder\\n      weight\\n      weightUnit\\n      condition\\n      status\\n      url\\n      needPrescription\\n      catalogID\\n      isLeasing\\n      isBlacklisted\\n      menu {\\n        id\\n        name\\n        url\\n        __typename\\n      }\\n      category {\\n        id\\n        name\\n        title\\n        breadcrumbURL\\n        isAdult\\n        isKyc\\n        minAge\\n        detail {\\n          id\\n          name\\n          breadcrumbURL\\n          isAdult\\n          __typename\\n        }\\n        __typename\\n      }\\n      txStats {\\n        transactionSuccess\\n        transactionReject\\n        countSold\\n        paymentVerified\\n        itemSoldFmt\\n        __typename\\n      }\\n      stats {\\n        countView\\n        countReview\\n        countTalk\\n        rating\\n        __typename\\n      }\\n      __typename\\n    }\\n    components {\\n      name\\n      type\\n      position\\n      data {\\n        ...ProductMedia\\n        ...ProductHighlight\\n        ...ProductInfo\\n        ...ProductDetail\\n        ...ProductSocial\\n        ...ProductDataInfo\\n        ...ProductCustomInfo\\n        ...ProductVariant\\n        __typename\\n      }\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\"}]";

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(uri)).setHeader("User-Agent",
                "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0")
            .setHeader("Accept", "*/*").setHeader("Accept-Language", "en-US,en;q=0.5")
            .setHeader("Referer",
                "https://www.tokopedia.com/khansaluxury/hanphone-nokia-lama?extParam=ivf%3Dfalse%26src%3Dsearch&refined=true")
            .setHeader("X-Tkpd-Lite-Service", "zeus").setHeader("X-Version", "d90a3a7")
            .setHeader("content-type", "application/json").setHeader("x-device", "desktop")
            .setHeader("X-TKPD-AKAMAI", "pdpGetLayout").setHeader("X-Source", "tokopedia-lite")
            .setHeader("Origin", "https://www.tokopedia.com").setHeader("Cookie",
                "_UUID_NONLOGIN_=1b0c7d353866c4b6fb4594429dd82840; _UUID_NONLOGIN_.sig=pMwsTcCPPfHul8vamOg3DutjKBk; _SID_Tokopedia_=U1u-pagjvooIWo-S1pgclmwvYENhZDBIPx0hwNoXUkyEq3pAQrlZWom95SF8Tz-wU7NIE6emYSJTuFiiyf3y15wJpHayq-5Xxj2jOiY9-4s3yp-V-bhvXDSxnQJZW8_z; DID=0d7e3ca282fa16886ef73b7659a49f225ed260cb325b0a99c85a9a1b7e4282a942d32d5f246f335b3c27f3b70cbd1bbb; DID_JS=MGQ3ZTNjYTI4MmZhMTY4ODZlZjczYjc2NTlhNDlmMjI1ZWQyNjBjYjMyNWIwYTk5Yzg1YTlhMWI3ZTQyODJhOTQyZDMyZDVmMjQ2ZjMzNWIzYzI3ZjNiNzBjYmQxYmJi47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=; _gcl_au=1.1.964979018.1661622022; _ga_70947XW48P=GS1.1.1661659889.3.1.1661661028.51.0.0; _ga=GA1.2.1228170133.1661622023; _gid=GA1.2.748940209.1661622023; _UUID_CAS_=524cde0a-7cdc-40f6-87a2-2a5442b86e65; _CASE_=7e27614c61273f373732312927644c61273f352927696769273f274f646e64777164255570766471272927664c61273f3432332927696a6b62273f27272927696471273f2727292775466a273f27272927724c61273f34373734353632302927764c61273f3434303635303236292776517c7560273f27376d272927726d76273f275e7e5927726477606d6a7076605a6c6159273f3437373435363230295927766077736c66605a717c756059273f5927376d59272959275a5a717c75606b64686059273f5927526477606d6a70766076592778297e5927726477606d6a7076605a6c6159273f35295927766077736c66605a717c756059273f592734306859272959275a5a717c75606b64686059273f5927526477606d6a707660765927785827292769507561273f273735373728353d28373d5135353f31353f37362e35323f35352778; _jxx=5935cd50-262f-11ed-82cc-d9794eebe228; _jx=5935cd50-262f-11ed-82cc-d9794eebe228; _abck=8BA2B7FE27DE2FBE16FF1444F3C9FCD8~0~YAAQb8MmF3kYStiCAQAAQi604gjqlX+j+9EPEHUoCfUWa5Hbxdik88GrGdxvrbt0v8c4A29u277l1cKGJzxf8Uo4sLhq4gTRd2fYqJUdQsiB3uEKBwt9RAUPSPUxwvxdewF1VM9wLFriPS38wOd98YF6G8gSFQX70VgbhuboJqePeUqMsPe9fzB6fTGjr5PhI5U+Mz1FU2+yWmACiAfc2NDdo18GfMECZup1P+K5/qyLSfP6CfCKD83LRrRFC8NYno5P7RlyxqMeLW4hHlQiHjAn3Z38MNy9i9/15sXc9FLPcWsQnX8nHXLKKYD47JnFuOQ8jFSSRI2gWotjsyn4UhwGmf0P53Va6sJ/VYE3znfWZ4DO/YZxsbuXNQMS9+NvV57woBc+Fsr2v3wzVagGlb478+7SSy+TmD8w~-1~-1~-1; hfv_banner=true; bm_sz=38D6303ADBF6FDF236E9B53C705CF651~YAAQfcMmF7566NyCAQAA99GQ4hB0TTldV4kO0f0taHOAPAEAucMjN0e+pikBZ/DgNDbYn9dJQ1izgjwRjzNJsCOt14vdSy877w2ivyM2KxURzZA7I8rEOzRSztdyPaLnHfrYlO3kUGj6wnWvlwKBbXf9+PsXDlvkhA84jsAPJySWGtG8+Y/ajEGHnM62ZmTJJUwsXUN5okgZwIx8EbBbWdTC0P9E5H94Bk/TMYVR5DjD2uVB6OLMaXms1UF4M25IqztEu+dHYzv0oSi7gH7O8HCbueKJ921GzW6qs+Ptb8KhAeSSnBg=~4337989~3290435; ak_bmsc=23BE64C38D30A6804D12D2BF77DAEF11~000000000000000000000000000000~YAAQ7+F4ct/3nOKCAQAAtM+s4hAdVVk2OqlaUAP9FuqiDjL5jMhXnc3igHxC1hDso5UEp2VTue3WV/3hd/8MsHd9Saeg+6IXqka/jo530eA24GKHwBDiHVs4nlccHnxIf/Jp7Ht8YCPkH21FtbhlK1SpiODNxh2Dhp9r4qaS0YvSrsPFsKvSta6+utp3irxx5GULT+u01nsxMR7FeCagwjYHKg3FzkXPV+FgrdbqfakEkS6uw1XSdiNTngDsw2rPxp3cGNKJyHGcKdgsB6pdBB+TCtqTe7zNox0yKVMo7ZG5v7023lmlKPx7YXO32LQM/ICc6kh674DLavZiLNynFDVyMb4l6ysHn4oZCJ1+QSTV91JlHKFC2Ds6S3lRtVZinJBS2IRYH1znxOIQWtfP1rF8kqV+0N1vGW0zBmOl4h7C0y6lWdVjq9B6qbUYQjBhkyPqWRk0B7+IBs6JKUAQ8d3XYJwSVfPbcS5B+LlEJt2tg7bJHThVbinxn0qwlst/; _jxxs=1661660358-5935cd50-262f-11ed-82cc-d9794eebe228; _jxs=1661660358-5935cd50-262f-11ed-82cc-d9794eebe228; _gat_UA-9801603-1=1; _dc_gtm_UA-126956641-6=1; _dc_gtm_UA-9801603-1=1; _abck=8BA2B7FE27DE2FBE16FF1444F3C9FCD8~-1~YAAQfcMmFxjE6tyCAQAA/PvB4ghE3vI74xCL0B5CQOiOaCJ3VLu97Co/nbi/D/eMlSO1I5p1ycwgrUr6K+DD1AkbfLMj4SgkbqnlyaZZrMlIWu6wpHkkRWEZNSsTpjdbBOkp9EcCLnuPRAz//hhcNfzHl/cSFcR5DVUHqUkkKZwDiWzN6qOu6DU9dHoUqIwU05u2bf2XRiqa1NXOYwmC/Lpe8UIhRRQB+xSCMwjHXdt0eTmLExrGg64Ay6TcitiW7ca1GZpmS8RzhItnhvLjA9nXQmICG7Pp+VzQicR4tUGttlzogYOortCO7pRkTqqnM7V33dWJsd8K3ICXdi2TgaFIYbLAELrtjSpHavT6W77MLjPL1MMTLjuPR2k0ZJ1DRPQkKwlL2+XTHykSaEr1f3hMGdi1lSzw4PMJ~0~-1~-1")
            .setHeader("Sec-Fetch-Dest", "empty").setHeader("Sec-Fetch-Mode", "cors")
            .setHeader("Sec-Fetch-Site", "same-site").setHeader("TE", "trailers")
            .POST(HttpRequest.BodyPublishers.ofString(payload))
            .timeout(Duration.of(30, ChronoUnit.SECONDS)).build();

        try {
            HttpResponse<String> response = httpClient.send(request,
                HttpResponse.BodyHandlers.ofString());

            return getDescriptionProduct(response.body());
        } catch (IOException | InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new GeneralExceptions(e.getMessage(), e);
        }
    }

    private static String getDescriptionProduct(String response) {
        ObjectMapper mapper = new ObjectMapper().configure(
            DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try {
            DetailProduct[] detailProducts = mapper.readValue(response, DetailProduct[].class);

            List<List<Component>> componentsList =
                Arrays.stream(detailProducts)
                    .map(detailProduct -> detailProduct.getData().getPdpGetLayout().getComponents())
                    .collect(Collectors.toList());

            for (List<Component> components : componentsList) {
                Component component =
                    components.stream().filter(c -> "product_detail".equals(c.getName()))
                        .findFirst().orElse(null);

                if (component != null) {
                    List<Map<String, Object>> contents = mapper.convertValue(component.getData(),
                        List.class);

                    for (Map<String, Object> content : contents) {
                        List<Map<String, Object>> c = mapper.convertValue(content.get("content"),
                            List.class);

                        Map<String, Object> val =
                            c.stream().filter(
                                    dt -> "Deskripsi".equalsIgnoreCase(dt.get("title").toString()))
                                .findFirst().orElse(null);

                        if (val != null) {
                            return val.get("subtitle").toString();
                        }
                    }
                }
            }

            return null;

        } catch (JsonProcessingException e) {
            throw new GeneralExceptions(e.getMessage(), e);
        }
    }

    private static void exportCsv(List<String[]> data) {
        try (ICSVWriter writer = new CSVWriterBuilder(
            new FileWriter("top_phone.csv")).withSeparator(';').build()) {
            writer.writeAll(data);
        } catch (IOException e) {
            throw new GeneralExceptions(e.getMessage(), e);
        }
    }

}