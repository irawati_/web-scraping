package ira.scraping.tokped.exceptions;

public class GeneralExceptions extends RuntimeException {

    public GeneralExceptions(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

}
