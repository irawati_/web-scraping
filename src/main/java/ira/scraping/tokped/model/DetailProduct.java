package ira.scraping.tokped.model;

import java.util.List;
import lombok.Data;

@Data
public class DetailProduct {


    private Detail data;

    @Data
    public static class Detail {

        private PdpGetLayout pdpGetLayout;
    }

    @Data
    public static class PdpGetLayout {

        private List<Component> components;
    }

    @Data
    public static class Component {

        private String name;
        private List<Object> data;
    }


    @Data
    public static class Content {

        private String title;
        private String subTitle;
    }

}
