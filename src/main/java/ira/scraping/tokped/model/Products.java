package ira.scraping.tokped.model;

import lombok.Data;

@Data
public class Products {

    private Long id;
    private String name;
    private String imageUrl;
    private String price;
    private Integer rating;
    private Shop shop;
    private String url;

    @Data
    public static class Shop {

        private String name;

    }
}
