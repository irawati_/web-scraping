package ira.scraping.tokped.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DataResponse {

    @JsonProperty("ace_search_product_v4")
    private AceSearchProductV4 aceSearchProductV4;

}
