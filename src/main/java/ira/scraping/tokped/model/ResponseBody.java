package ira.scraping.tokped.model;

import lombok.Data;

@Data
public class ResponseBody {

    private DataResponse data;

}
